//
//  LiveDataWidgetView.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation
import UIKit

class ErrorView: UIView {
    @IBOutlet var retryButton: UIButton!
    @IBOutlet var errorLabel: UILabel!
}

final class LiveDataWidgetView: UIView {
    @IBOutlet var solarView: NumberUnitView!
    @IBOutlet var gridView: NumberUnitView!
    @IBOutlet var buildingDemandView: NumberUnitView!
    @IBOutlet var quasarsView: NumberUnitView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet var errorController: ErrorController!

    var state: State<LiveData> = .loading {
        didSet {
            updateState(state)
        }
    }

    var liveData: LiveData = .init() {
        didSet {
            updateLiveData(liveData)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        emsLoadFromXib()
    }

    private func updateState(_ state: State<LiveData>) {
        switch state {
        case .loading:
            activityIndicatorView.startAnimating()
            errorController.hideError()
        case let .data(liveData):
            stopLoading()
            errorController.hideError()
            self.liveData = liveData
        case let .error(error):
            activityIndicatorView.stopAnimating()
            errorController.showError(error)
        }
    }

    func updateLiveData(_ liveData: LiveData) {
        solarView.fillNumber(liveData.solarPower)
        gridView.fillNumber(liveData.gridPower)
        buildingDemandView.fillNumber(liveData.buildingDemand)
        quasarsView.fillNumber(liveData.quasarsPower)
    }

    private func stopLoading() {
        activityIndicatorView.stopAnimating()
        [solarView, gridView, buildingDemandView, quasarsView]
            .forEach { $0?.isHidden = false }
    }
}
