//
//  QuasarWidgetView.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import UIKit

class QuasarWidgetView: UIView {
    @IBOutlet var chargedView: NumberUnitView!
    @IBOutlet var dischargedView: NumberUnitView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet var errorController: ErrorController!

    var state: State<QuasarCharger> = .loading {
        didSet {
            updateState(state)
        }
    }

    var quasar: QuasarCharger? {
        didSet {
            if let quasar = quasar {
                updateQuasar(quasar)
            }
        }
    }

    private func updateState(_ state: State<QuasarCharger>) {
        switch state {
        case .loading:
            activityIndicatorView.startAnimating()
            errorController.hideError()
        case let .data(quasar):
            stopLoading()
            errorController.hideError()
            self.quasar = quasar
        case let .error(error):
            activityIndicatorView.stopAnimating()
            errorController.showError(error)
        }
    }

    func updateQuasar(_ quasar: QuasarCharger) {
        chargedView.fillNumber(quasar.energyCharged)
        dischargedView.fillNumber(quasar.energyDischarged)
    }

    private func stopLoading() {
        activityIndicatorView.stopAnimating()
        chargedView.isHidden = false
        dischargedView.isHidden = false
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        emsLoadFromXib()
    }
}
