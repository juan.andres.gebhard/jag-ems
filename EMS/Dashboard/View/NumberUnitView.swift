//
//  NumberUnitView.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation
import UIKit

final class NumberUnitView: UIView {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var unitLabel: UILabel!

    @IBInspectable var unit: String = "" {
        didSet {
            unitLabel.text = unit
        }
    }

    @IBInspectable var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }

    var numberFormatter = NumberFormatter()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupNumberFormatter()
        emsLoadFromXib()
    }

    func setupNumberFormatter() {
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
    }

    func fillNumber(_ number: Double?) {
        if let number = number {
            numberLabel.text = numberFormatter.string(from: NSNumber(value: number))
        }
    }
}
