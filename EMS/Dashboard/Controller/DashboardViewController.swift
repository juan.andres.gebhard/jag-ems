//
//  ViewController.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 31/03/2022.
//

import UIKit

class DashboardViewController: UIViewController {
    @IBOutlet var quasarWidgetView: QuasarWidgetView!
    @IBOutlet var liveDataWidgetView: LiveDataWidgetView!
    @IBOutlet var statisticsViewController: StatisticsWidgetViewController!
    @IBOutlet var scrollView: UIScrollView!
    let refreshControl = UIRefreshControl()

    let kDetailSegueId = "Detail"

    var energyService: EnergyService = LocalFileEnergyService()

    var history: [HistorySample]?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setupRefreshControl()
    }

    private func setupRefreshControl() {
        scrollView.refreshControl = refreshControl
        refreshControl.addTarget(self,
                                 action: #selector(loadData),
                                 for: .valueChanged)
    }

    @objc private func loadData() {
        refreshControl.beginRefreshing()
        updateLiveData()
        updateQuasarCharger()
    }

    private func updateLiveData() {
        energyService.fetchLiveData { (result: Result<LiveData, Error>) in
            switch result {
            case let .success(liveData):
                self.liveDataWidgetView.state = .data(liveData)
                self.statisticsViewController.state = .data(liveData)
                self.refreshControl.endRefreshing()
            case let .failure(error):
                self.liveDataWidgetView.state = .error(error)
                self.statisticsViewController.state = .error(error)
                self.refreshControl.endRefreshing()
            }
        }
    }

    private func updateQuasarCharger() {
        energyService.fetchHistory { (result: Result<[HistorySample], Error>) in
            switch result {
            case let .success(history):
                let quasar = QuasarCharger(history: history)
                self.quasarWidgetView.state = .data(quasar)
                self.history = history
            case let .failure(error):
                self.quasarWidgetView.state = .error(error)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "Statistics",
           let viewController = segue.destination as? StatisticsWidgetViewController
        {
            statisticsViewController = viewController
        } else if segue.identifier == kDetailSegueId,
                  let viewController = segue.destination as? DetailViewController,
                  let history = history
        {
            viewController.history = history
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender _: Any?) -> Bool {
        if identifier == kDetailSegueId {
            if let history = history, !history.isEmpty {
                return true
            }
            return false
        }
        return true
    }
}
