//
//  SourcesPercentagesViewController.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 04/04/2022.
//

import UIKit

class StatisticsWidgetViewController: UIViewController {
    @IBOutlet var solarView: NumberUnitView!
    @IBOutlet var quasarView: NumberUnitView!
    @IBOutlet var gridView: NumberUnitView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet var errorController: ErrorController!

    var state: State<LiveData> = .loading {
        didSet {
            updateState(state)
        }
    }

    var liveData: LiveData? {
        didSet {
            if let liveData = liveData {
                updateViews(liveData: liveData)
            }
        }
    }

    private func updateState(_ state: State<LiveData>) {
        switch state {
        case .loading:
            activityIndicatorView.startAnimating()
            errorController.hideError()
        case let .data(liveData):
            stopLoading()
            errorController.hideError()
            self.liveData = liveData
        case let .error(error):
            activityIndicatorView.stopAnimating()
            errorController.showError(error)
        }
    }

    func updateViews(liveData: LiveData) {
        solarView.fillNumber(percent(part: liveData.solarPower,
                                     total: liveData.buildingDemand))
        gridView.fillNumber(percent(part: liveData.gridPower,
                                    total: liveData.buildingDemand))
        quasarView.fillNumber(percent(part: liveData.quasarsPower,
                                      total: liveData.buildingDemand))
    }

    private func stopLoading() {
        activityIndicatorView.stopAnimating()
        [solarView, gridView, quasarView].forEach { $0?.isHidden = false }
    }

    func percent(part: Double?, total: Double?) -> Double {
        if let part = part, let total = total {
            return abs(part) * 100.0 / total
        }
        return 0
    }
}
