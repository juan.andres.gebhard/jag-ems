//
//  ErrorController.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

import Foundation
import UIKit

final class ErrorController: NSObject {
    @IBOutlet var view: UIView!

    lazy var errorView: ErrorView = Bundle.main.loadNibNamed(String(describing: ErrorView.self),
                                                             owner: self,
                                                             options: nil)?.first as! ErrorView

    func showError(_ error: Error) {
        errorView.errorLabel.text = error.localizedDescription
        view.addSubview(errorView)
        NSLayoutConstraint.activate([
            view.centerXAnchor.constraint(equalTo: errorView.centerXAnchor),
            view.centerYAnchor.constraint(equalTo: errorView.centerYAnchor),
            view.leadingAnchor.constraint(greaterThanOrEqualTo: errorView.leadingAnchor, constant: 16),
            view.trailingAnchor.constraint(greaterThanOrEqualTo: errorView.trailingAnchor, constant: 16),
        ])
    }

    func hideError() {
        errorView.removeFromSuperview()
    }
}
