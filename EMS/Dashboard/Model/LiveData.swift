//
//  LiveData.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation

struct LiveData: Codable, Equatable {
    var solarPower: Double?
    var quasarsPower: Double?
    var gridPower: Double?
    var buildingDemand: Double?
    var systemSoc: Double?
    var totalEnergy: Double?
    var currentEnergy: Double?
}
