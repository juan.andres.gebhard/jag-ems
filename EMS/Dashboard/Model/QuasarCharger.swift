//
//  QuasarCharger.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation

/// Model for the quasar charger.
struct QuasarCharger: Equatable {
    /// Creates an instance of a quasar charger
    /// - Parameter history: an array of history samples used to calculate the total amount of energy charged and discharged
    init(history: [HistorySample]) {
        (energyCharged, energyDischarged) = QuasarCharger.calculateChargeAndDischarge(history: history)
    }

    /// The amount of energy discharged from the Quasar charger in kWh
    let energyCharged: Double

    /// The amount of energy charged from the Quasar charger in kWh
    let energyDischarged: Double

    private static func calculateChargeAndDischarge(history: [HistorySample]) -> (charged: Double, discharged: Double) {
        var chargedSum = 0.0
        var dischargedSum = 0.0
        var chargedCount = 0
        var dischargedCount = 0
        var totalCharged = 0.0
        var totalDischarged = 0.0

        history.forEach { sample in
            let power = sample.quasarsActivePower
            if power > 0 {
                chargedSum += power
                chargedCount += 1
            } else if power < 0 {
                dischargedSum += power
                dischargedCount += 1
            }
        }

        if chargedCount > 0 {
            totalCharged = chargedSum / Double(chargedCount) / Double(60)
        }
        if chargedCount > 0 {
            totalDischarged = dischargedSum / Double(dischargedCount) / Double(60)
            totalDischarged *= -1
        }

        return (totalCharged, totalDischarged)
    }
}
