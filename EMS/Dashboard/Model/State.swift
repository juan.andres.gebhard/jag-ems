//
//  State.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

import Foundation

enum State<DataType: Equatable>: Equatable {
    static func == (lhs: State<DataType>, rhs: State<DataType>) -> Bool {
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case let (.data(lData), .data(rData)):
            return lData == rData
        case let (.error(lError), .error(rError)):
            return type(of: lError) == type(of: rError)
        default:
            return false
        }
    }

    case loading
    case data(DataType)
    case error(Error)
}
