//
//  History.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation

struct History {
    var samples: [HistorySample]
}
