//
//  HistorySample.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation

struct HistorySample: Codable, Equatable {
    var buildingActivePower: Double
    var gridActivePower: Double
    var pvActivePower: Double
    var quasarsActivePower: Double
    var timestamp: Date
}
