//
//  UILabel+Format.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation
import UIKit

class LabelStyle: NSObject {
    var size: CGFloat = 24
    var color = UIColor.black
    var fontName = "Helvetica Neue"
    @IBOutlet var label: UILabel? {
        didSet {
            if let label = label {
                label.font = UIFont(name: fontName, size: size)
                label.textColor = color
            }
        }
    }
}

class TitleStyle: LabelStyle {
    override init() {
        super.init()
        fontName = "HelveticaNeue-Bold"
    }
}

class DataTitleStyle: LabelStyle {
    override init() {
        super.init()
        size = 16
        color = UIColor.gray
    }
}

class NumberStyle: LabelStyle {
    override init() {
        super.init()
        size = 32
    }
}

class UnitStyle: LabelStyle {
    override init() {
        super.init()
        size = 22
    }
}
