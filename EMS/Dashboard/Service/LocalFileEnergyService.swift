//
//  MockEnergyService.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation

final class LocalFileEnergyService: EnergyService {
    var timesLiveData = 0

    func fetchHistory(completion: @escaping (Result<[HistorySample], Error>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.fetchFromFile(fileName: "historic_data", completion: completion)
        }
    }

    func fetchLiveData(completion: @escaping (Result<LiveData, Error>) -> Void) {
        let fileName = timesLiveData == 0 ? "live_data2" : "live_data"
        timesLiveData += 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.fetchFromFile(fileName: fileName, completion: completion)
        }
    }

    func fetchFromFile<DataType: Codable>(fileName: String,
                                          completion: @escaping (Result<DataType, Error>) -> Void)
    {
        let url = Bundle.main.url(forResource: fileName, withExtension: "json")
        guard let url = url else {
            completion(Result.failure(ServiceError.resourceNotFound))
            return
        }
        do {
            let data = try Data(contentsOf: url)
            completion(Parser<DataType>().parse(data: data))
        } catch {
            completion(Result.failure(error))
        }
    }
}
