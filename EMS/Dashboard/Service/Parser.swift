//
//  Parser.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 03/04/2022.
//

import Foundation

final class Parser<DataType: Codable> {
    func parse(data: Data) -> Result<DataType, Error> {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .iso8601
        do {
            let object = try decoder.decode(DataType.self, from: data)
            return Result.success(object)
        } catch {
            return Result.failure(error)
        }
    }
}
