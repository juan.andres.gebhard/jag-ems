//
//  EnergyService.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 02/04/2022.
//

import Foundation

enum ServiceError: Error {
    case resourceNotFound
}

protocol EnergyService {
    func fetchLiveData(completion: @escaping (Result<LiveData, Error>) -> Void)

    func fetchHistory(completion: @escaping (Result<[HistorySample], Error>) -> Void)
}
