//
//  DetailViewController.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 05/04/2022.
//

import Foundation
import SwiftChart
import UIKit

final class DetailViewController: UIViewController {
    @IBOutlet var chart: Chart!

    /// trailing constraint for the references view when it's in h:compact size classes
    @IBOutlet var referencesTrailing: NSLayoutConstraint!
    private let collapsedReference = CGFloat(-220)
    private let expandedReference = CGFloat(32)

    /// Views that show the color of each source
    @IBOutlet var solarColorView: UIView!
    @IBOutlet var quasarColorView: UIView!
    @IBOutlet var buildingColorView: UIView!
    @IBOutlet var gridColorView: UIView!

    var history: [HistorySample]? {
        didSet {
            dataSource.history = history
        }
    }

    var dataSource: DetailDataSource = DefaultDetailDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupChart()
        setupReferences()
    }

    private func setupChart() {
        chart.add(dataSource.series)
        chart.xLabels = dataSource.xLabels
        chart.xLabelsFormatter = dataSource.xLabelsFormatter
        chart.xLabelsOrientation = .horizontal
        chart.labelFont = UIFont.systemFont(ofSize: 8)
    }

    private func setupReferences() {
        [solarColorView: dataSource.solarColor,
         quasarColorView: dataSource.quasarColor,
         buildingColorView: dataSource.buildingColor,
         gridColorView: dataSource.gridColor]
            .forEach { (key: UIView?, value: UIColor) in
                key?.backgroundColor = value
            }
    }

    @IBAction func referencesAction() {
        referencesTrailing.constant = referencesTrailing.constant == expandedReference ? collapsedReference : expandedReference
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }

        if traitCollection.verticalSizeClass == .regular {
            let alert = UIAlertController(title: "Hint", message: "Use this button to show or hide the references when the device is in landscape mode.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
}
