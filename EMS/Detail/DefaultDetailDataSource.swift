//
//  DetailDataSource.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

import Foundation
import SwiftChart

final class DefaultDetailDataSource: DetailDataSource {
    var series: [ChartSeries] = []
    var xLabels: [Double]?
    var xLabelsFormatter = { (_: Int, labelValue: Double) -> String in
        String(Int(labelValue))
    }

    var solarColor = UIColor.systemPink
    var gridColor = UIColor.purple
    var quasarColor = UIColor.systemTeal
    var buildingColor = UIColor.blue

    private let maxXLabels: Int = 4

    var history: [HistorySample]? {
        didSet {
            if let history = history {
                updateSeries(history: history)
                updateXLabels(history: history)
            }
        }
    }

    private func updateSeries(history: [HistorySample]) {
        var buildingData: [(x: Int, y: Double)] = []
        var gridData: [(x: Int, y: Double)] = []
        var solarData: [(x: Int, y: Double)] = []
        var quasarData: [(x: Int, y: Double)] = []

        history.enumerated().forEach { index, sample in
            buildingData.append((x: index, y: sample.buildingActivePower))
            gridData.append((x: index, y: sample.gridActivePower))
            solarData.append((x: index, y: sample.pvActivePower))
            quasarData.append((x: index, y: sample.quasarsActivePower))
        }

        series = [
            (buildingData, buildingColor),
            (gridData, gridColor),
            (solarData, solarColor),
            (quasarData, quasarColor),
        ].map { (data: [(x: Int, y: Double)], color: UIColor) -> ChartSeries in
            let serie = ChartSeries(data: data)
            serie.color = color
            return serie
        }
    }

    private func updateXLabels(history: [HistorySample]) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E HH:mm"
        xLabelsFormatter = { _, value -> String in
            dateFormatter.string(from: history[Int(value)].timestamp)
        }

        if history.count > maxXLabels {
            let step = history.count / maxXLabels
            var labels: [Double] = []
            for i in 0 ..< maxXLabels {
                labels.append(Double(i * step))
            }
            xLabels = labels
        }
    }
}
