//
//  DetailDataSource.swift
//  EMS
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

import Foundation
import SwiftChart
import UIKit

protocol DetailDataSource {
    var series: [ChartSeries] { get }
    var xLabels: [Double]? { get }
    var xLabelsFormatter: (Int, Double) -> String { get }
    var solarColor: UIColor { get }
    var gridColor: UIColor { get }
    var quasarColor: UIColor { get }
    var buildingColor: UIColor { get }

    var history: [HistorySample]? { get set }
}
