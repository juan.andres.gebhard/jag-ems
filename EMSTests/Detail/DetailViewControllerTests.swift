//
//  DetailViewControllerTests.swift
//  EMSTests
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

@testable import EMS
import XCTest

class DetailViewControllerTests: XCTestCase {
    var storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    var viewController: DetailViewController!

    override func setUpWithError() throws {
        try super.setUpWithError()
        viewController = (storyboard.instantiateViewController(withIdentifier: "Detail") as! DetailViewController)
    }

    func testInit_UsesDefaultDataSource() {
        XCTAssert(viewController.dataSource is DefaultDetailDataSource)
    }

    func testViewDidLoad_SetupsXLabels_Horizontal() {
        viewController.history = []

        _ = viewController.view

        XCTAssertEqual(viewController.chart.xLabelsOrientation, .horizontal)
    }

    func testReferencesAction_verticalCompactSizeClass_TooglesViewVisibility() {
        _ = viewController.view

        viewController.referencesAction()
        XCTAssertGreaterThan(viewController.referencesTrailing.constant, 0)

        viewController.referencesAction()
        XCTAssertLessThan(viewController.referencesTrailing.constant, 0)
    }

    func testReferencesAction_verticalRegularSizeClass_ShowsAlertController() {
        _ = viewController.view
        UIApplication.shared.keyWindow?.rootViewController = viewController

        if viewController.traitCollection.verticalSizeClass == .regular {
            viewController.referencesAction()

            if let alert = viewController.presentedViewController as? UIAlertController {
                XCTAssertEqual(alert.title, "Hint")
            } else {
                XCTAssert(false)
            }
        }
    }
}
