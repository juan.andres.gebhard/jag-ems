//
//  DashboardViewControllerTests.swift
//  EMSTests
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

@testable import EMS
import XCTest

private class MockEnergyService: EnergyService {
    var didCallFetchLiveData = false
    var didCallFetchHistory = false
    var liveData = LiveData()
    var history: [HistorySample] = []

    func fetchLiveData(completion: @escaping (Result<LiveData, Error>) -> Void) {
        didCallFetchLiveData = true
        completion(.success(liveData))
    }

    func fetchHistory(completion: @escaping (Result<[HistorySample], Error>) -> Void) {
        didCallFetchHistory = true
        completion(.success(history))
    }
}

private class NullService: EnergyService {
    func fetchLiveData(completion _: @escaping (Result<LiveData, Error>) -> Void) {}

    func fetchHistory(completion _: @escaping (Result<[HistorySample], Error>) -> Void) {}
}

private class FailureEnergyService: EnergyService {
    var error = ServiceError.resourceNotFound

    func fetchLiveData(completion: @escaping (Result<LiveData, Error>) -> Void) {
        completion(.failure(error))
    }

    func fetchHistory(completion: @escaping (Result<[HistorySample], Error>) -> Void) {
        completion(.failure(error))
    }
}

class DashboardViewControllerTests: XCTestCase {
    var viewController: DashboardViewController!
    var storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

    override func setUp() {
        super.setUp()
        viewController = (storyboard.instantiateViewController(withIdentifier: "Dashboard") as! DashboardViewController)
    }

    func testViewDidLoad_WidgetsAreCreatedWithLoadingState() {
        let service = NullService()
        viewController.energyService = service

        _ = viewController.view

        XCTAssertEqual(viewController.quasarWidgetView.state,
                       State.loading)
        XCTAssertEqual(viewController.liveDataWidgetView.state,
                       State.loading)
        XCTAssertEqual(viewController.statisticsViewController.state,
                       State.loading)
    }

    // MARK: Live data

    func testViewDidLoad_FetchesLiveDataAndHistory() throws {
        let service = MockEnergyService()
        viewController.energyService = service

        let _ = viewController.view

        XCTAssert(service.didCallFetchLiveData)
        XCTAssert(service.didCallFetchHistory)
    }

    func testLiveDataFetch_OnSuccess_UpdatesLiveDataAndStatisticsWidget() {
        let service = MockEnergyService()
        viewController.energyService = service

        _ = viewController.view

        XCTAssertEqual(viewController.liveDataWidgetView.state,
                       State.data(service.liveData))
        XCTAssertEqual(viewController.statisticsViewController.state,
                       State.data(service.liveData))
    }

    func testLiveDataFetch_OnFailure_UpdatesLiveDataAndStatisticsWidgetWithErrorState() {
        let service = FailureEnergyService()
        viewController.energyService = service

        _ = viewController.view

        XCTAssertEqual(viewController.liveDataWidgetView.state,
                       State.error(service.error))
        XCTAssertEqual(viewController.statisticsViewController.state,
                       State.error(service.error))
    }

    // MARK: History

    func testHistoryFetch_OnSuccess_UpdatesQuasarWidget() {
        let service = MockEnergyService()
        viewController.energyService = service
        let charger = QuasarCharger(history: service.history)

        _ = viewController.view

        XCTAssertEqual(viewController.quasarWidgetView.state,
                       State.data(charger))
    }

    func testHistoryFetch_OnFailure_UpdatesQuasarWidgetWithErrorState() {
        let service = FailureEnergyService()
        viewController.energyService = service

        _ = viewController.view

        XCTAssertEqual(viewController.quasarWidgetView.state,
                       State.error(service.error))
    }

    func testPrepareForSegue_WithDetail_SetsHistory() {
        let service = MockEnergyService()
        _ = viewController.view
        viewController.history = service.history
        let detailViewController = storyboard.instantiateViewController(withIdentifier: "Detail") as! DetailViewController
        let segue = UIStoryboardSegue(identifier: "Detail", source: viewController, destination: detailViewController)

        viewController.prepare(for: segue, sender: nil)

        XCTAssertEqual(viewController.history, detailViewController.history)
    }
}
