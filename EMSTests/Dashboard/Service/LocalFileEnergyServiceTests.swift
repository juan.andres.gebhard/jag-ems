//
//  LocalFileEnergyServiceTests.swift
//  EMSTests
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

@testable import EMS
import XCTest

class LocalFileEnergyServiceTests: XCTestCase {
    func testFetchLiveData_FirstTime_Fails() {
        let service = LocalFileEnergyService()
        let expectation = XCTestExpectation(description: "fetchLiveData should fail the first time")

        service.fetchLiveData { (result: Result<LiveData, Error>) in
            switch result {
            case .success:
                break
            case .failure:
                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 3)
    }

    func testFetchLiveData_SecondTime_Success() {
        let service = LocalFileEnergyService()
        let expectation = XCTestExpectation(description: "fetchLiveData should succeed the second time")

        service.fetchLiveData { _ in }
        service.fetchLiveData { (result: Result<LiveData, Error>) in
            switch result {
            case .success:
                expectation.fulfill()
            case .failure:
                break
            }
        }

        wait(for: [expectation], timeout: 3)
    }

    func testFetchLiveData_OnSuccess_ParsesObjectCorrecty() {
        let service = LocalFileEnergyService()
        let expectation = XCTestExpectation(description: "parse is successfull")

        service.fetchLiveData { _ in }
        service.fetchLiveData { (result: Result<LiveData, Error>) in
            switch result {
            case let .success(liveData):
                if liveData.quasarsPower == -38.732,
                   liveData.buildingDemand == 127.03399999999999,
                   liveData.solarPower == 7.827,
                   liveData.gridPower == 80.475,
                   liveData.totalEnergy == 960
                {
                    expectation.fulfill()
                }
            case .failure:
                break
            }
        }

        wait(for: [expectation], timeout: 3)
    }

    func testFetchHistory_OnSuccess_ParsesObjectCorrecty() {
        let service = LocalFileEnergyService()
        let expectation = XCTestExpectation(description: "Parse is successfull")

        service.fetchHistory { (result: Result<[HistorySample], Error>) in
            switch result {
            case let .success(history):
                if history.count == 1125 {
                    expectation.fulfill()
                }
            case .failure:
                break
            }
        }

        wait(for: [expectation], timeout: 3)
    }
}
