//
//  ParserTests.swift
//  EMSTests
//
//  Created by Juan Andres Gebhard on 06/04/2022.
//

@testable import EMS
import XCTest

struct TestObject: Codable {
    var text: String
}

class ParserTests: XCTestCase {
    func testParse_WithCorrectJSON_ReturnsObject() {
        let parser = Parser<TestObject>()

        let result = parser.parse(data: correctData())

        switch result {
        case let .success(object):
            XCTAssertEqual(object.text, "Some text")
        case .failure:
            XCTAssert(false)
        }
    }

    func testParse_WithMissingText_ReturnsObject() {
        let parser = Parser<TestObject>()

        let result = parser.parse(data: incorrectData())

        switch result {
        case .success:
            XCTAssert(false)
        case let .failure(error):
            XCTAssert(error is DecodingError)
        }
    }

    func correctData() -> Data {
        return """
        {
            "text": "Some text"
        }
        """.data(using: .utf8)!
    }

    func incorrectData() -> Data {
        return """
        {
            "text": null
        }
        """.data(using: .utf8)!
    }
}
