# Starting

To start the project, open EMX.xcworkspace on Xcode 13.2.1 and hit Run.
On the main screen, two widgets are going to fail, that is intentional to show the error states. You should use the pull to refresh gesture the see all the widgets correctly. 

## Dependency Managment

This project uses Cocoapods. Yes, I decided *not* to ignore the Pods folder, that is why there is no need to run pod install.
I am well aware that ignoring the Pods folder is the most common practice and that there's good reason for that. I made this decision based on:
* *Integrity:* it is very important for me that this project meets a "clone & run" policy. If I want to make sure that it a possible to recreate a given version, then it is a wise choice to depend only on the one repository that I have under my control.
* *Few pods*: this project uses just one pod, storage space is not an issue.

# Main points

* More than 90% of unit testing coverage.
* The is no class with more than 100 LOC.
* Rotation supported.

# Architecture

This project uses the MVC architectural pattern. It is not a very popular or fashionable choice this days, mainly because it has earned a reputation a generating huge view controllers. However, if the developer focuses on applying the SOLID principles, that can be avoided in several ways, for example:
* the subviews can became new classes, 
* the view controller can be decomposed in smaller child view controller, 
* models and services can be helpful transforming the data,
* datasources and delegates can be separate classes.

I have considered other architectures, I would like to share a very short analysis.

> IMPORTANT NOTE: All architectures are great, they solve different problems and have a different set of pros and cons. This is just my analysis for this particular app, I'm *not* saying that the following architectures are bad.

## MVP / MVVM
Pros:
* The developer is forced to think the view in terms of what it can do.
* Since the Presenter or the ViewModel *has* to exists, the developer is encouraged to place some of logic in a class other than the view controller.
Cons:
* [MVP-only] The problem of a huge view controller is solved by cutting the class in half. If you had a god class of 200 LOC, now you have two classes of 100 lines. But it is only a matter of time until the class grows again and you have two classes of 200 LOC.
* It has an overhead of creating the view protocol (or implementing it via block or reactive programming). For a small app such as this one, it provides no additional benefit, i.e, I'm not going to reuse a viewmodel y for another view controller, I'm not having different views for the same presenter.

## VIPER
Pros:
* Forces the developer to think in terms of use cases, so it is easier to divide the N features in N classes.
* All the benefits of the clean code architecture.
Cons:
* A lot of boiler plate for a two-screen app.

# Design 

The project is organized by features. Inside each feature group, there are groups corresponding with the architecture: Models, Views, Controllers, Services, Styles.

## Dashboard

The dashboard has three widgets. The first two are implemented as UIViews. The third as a ViewController, that gets added as a child view controller.
All the widgets handle their state via the State enum, which has associated values, so you can easily pass the model/error when setting the state of the widget. 

![Dashboard views](diagrams/dashboard_views.png)

### Style

To keep the text style consistent across the widgets, there are several Style classes

![Style classes](diagrams/styles.png)

The child classes are really small and simple, they just redefine properties from the parent class.
This approach was selected because it allows to create the styles and assign them directly in the xib/storyboard files.

### Service

The service used to fetch the data is defined as a protocol. 

![Service](diagrams/service.png)

This allows to set a different service, which is useful for testing but it also makes it easier to create a service that requests the data to a server instead of a local file.
The asynchronous nature of the methods is handled with closures and the Result enum. The async/await produced simpler code but was harder to test for me.

The service requests create the LiveData and History models. The QuasarCharger model, however, is created in the view controller using the previously mentioned models.

## Detail

The detail chart uses the SwiftChart pod.
I created a DataSource even though SwiftChart doesn't require a datasource to work. 

![Detail](diagrams/detail.png)

The Detail screen shows a reference view when the vertical size class is Regular.
When the vertical size class is Compact, the reference view is hidden but can be displayed using the right button in the navigation bar.

# Known issues

There are some issues that I couldn't tackle due to time limits:

* Localization and i18n.
* The refresh control in the Dashboard works but the activity indicator is not being displayed properly when the user pulls.
* The coverage could be closer to 100%, there are very few lines that can't be tested.
* When the widget' state change, it very similar for each widget, there is a big opportunity for code reuse there. 

# Tooling

* Cocoapods for dependency management
* SwiftFormat for code formatting.

